/* 1.Создайте массив styles с элементами «Джаз» и «Блюз» */

let styles = ["Джаз ", " Блюз "];

document.write('1. ' + styles);
document.write('<br>' + '<hr>');

/* 2.Добавьте «Рок-н-ролл» в конец. */

styles.push(" Рок-н-ролл ");

document.write('2. ' + styles);
document.write('<br>' + '<hr>');

/* 3.Замените значение в середине на «Классика». Ваш код для поиска значения в середине должен работать для массивов с любой длиной. */ 
/* Будем использовать метод Math.floor(), который округляет аргумент до ближайшего меньшего целого. */
styles[Math.floor((styles.length - 1) / 2)] = " Классика ";

document.write('3. ' + styles);
document.write('<br>' + '<hr>');

/* 4.Удалите первый элемент массива и покажите его. */

alert(styles.shift());

document.write('4. ' + styles);
document.write('<br>' + '<hr>');

/* 5.Вставьте «Рэп» и «Регги» в начало массива. */

styles.unshift("Рэп ", " Регги ");

document.write('5. ' + styles);
document.write('<br>' + '<hr>');