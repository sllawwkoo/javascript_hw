//ещё один вариант решения
function map(fn, array) {
    let newArray = []; //создаём новый массив
    for (let i = 0; i < array.length; i++) { //Заполняем созданный массив обработанными элементами исходного массива 
        newArray[i] = fn(array[i]);       // и возвращаем его из функции 
    }
    return newArray;
}
function add(a) { //функция сложения
    return a + a;
}
document.write(map(add, [1, 2, 3, 4, 5])); // [2, 4, 6, 8, 10]