function map(fn, array) {
    let newArray = []; //создаём новый массив
    for (let i = 0; i < array.length; i++) { //Заполняем созданный массив обработанными элементами исходного массива 
        newArray.push(fn(array[i]));       // при помощи метода push (добавление новых элементов массива в конец) 
    }
    return newArray;
}
function add(a) { //функция сложения
    return a + a;
}
document.write(map(add, [1, 2, 3, 4, 5])); // [2, 4, 6, 8, 10]