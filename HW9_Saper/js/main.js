startGame();

/****** Функция запуска игры ******/
function startGame() {
    const field = document.querySelector('section');
    for (let i = 1; i <= 64; i++) {
        const cell = document.createElement('div');
        field.append(cell);
        cell.classList.add('cell')
    }

    const cells = [...field.children];

    let closedCount = 64;

    let countFlags = 10;

    let countClickLeft = 0;

    let firstClickBomb = [];

    /*** Генерируем 10 мин случайным образом ***/
    const bombs = [...Array(64).keys()].sort(() => Math.random() - 0.5).slice(0, 10);

    /*** Клик левой кнопки мыши ***/
    field.addEventListener('click', openCell);
    function openCell(event) {
        if (event.target.tagName !== 'DIV' ||
            event.target.getAttribute('class') === 'flaged') {
            return;
        }
        const index = cells.indexOf(event.target);
        const column = index % 8;
        const row = Math.floor(index / 8);
        open(row, column);
        countClickLeft++
        isFirstClickBomb()
    };

    /*** Блок кода с запуском таймера ***/
    const timer = document.querySelector('.num3');
    const number2 = document.querySelector('.num2');
    const number1 = document.querySelector('.num1');

    let counterTime = 0, intervalHandler;
    const countTime = () => {
        timer.textContent = counterTime;
        counterTime++
        if (timer.textContent > 9) {
            number2.style.display = 'none'
        };
        if (timer.textContent > 99) {
            number1.style.display = 'none'
        }
    };
    function startTimer() {
        if (countClickLeft === 1 && firstClickBomb.length !== 1) {
            intervalHandler = setInterval(countTime, 1000)
        }
    };
    function removeStartTimer() {
        field.removeEventListener('click', startTimer, false)
    };
    field.addEventListener('click', startTimer);
    field.addEventListener('click', removeStartTimer, false);
    /*********************** Конец Блока ***********************/

    /*** Функция для проверки первого клика, бомба или нет ***/
    function isFirstClickBomb() {
        cells.forEach((item) => {
            if (item.getAttribute('class') === 'bombed') {
                firstClickBomb.push(item)
            }
            return firstClickBomb
        })
    };
    /*** Функция для остановки таймера ***/
    function stopTimer() {
        clearInterval(intervalHandler)
    };

    /*** Клик правой кнопкой мыши (установить флажок или убрать его) ***/
    field.addEventListener('contextmenu', setFlag);
    function setFlag(event) {
        if (event.target.tagName !== 'DIV'
            || event.target.getAttribute('class') === 'one'
            || event.target.getAttribute('class') === 'two'
            || event.target.getAttribute('class') === 'three'
            || event.target.getAttribute('class') === 'four'
            || event.target.getAttribute('class') === 'five'
            || event.target.getAttribute('class') === 'six'
            || event.target.getAttribute('class') === 'seven'
            || event.target.getAttribute('class') === 'eight'
            || event.target.getAttribute('class') === 'zero'
            || event.target.getAttribute('class') === 'bomb'
            || event.target.getAttribute('class') === 'bombed') {
            return;
        };

        if (event.target.classList.contains('cell')) {
            event.target.classList.add('flaged')
            event.target.classList.remove('cell')
        }
        else if (event.target.classList.contains('flaged')) {
            event.target.classList.add('cell')
            event.target.classList.remove('flaged')
        }
        event.preventDefault();

    };

    /*** Счётчик флажков (мин) ***/
    const flags = document.querySelector('.countBomb');
    const firstNum = document.querySelector('.first');
    const secondNum = document.querySelector('.second');
    field.addEventListener('contextmenu', event => {
        if (event.target.getAttribute('class') === 'flaged') {
            countFlags--;
            if (countFlags < 10 && countFlags >= 0) {
                secondNum.textContent = '0'
                flags.textContent = firstNum.textContent + secondNum.textContent + countFlags
            } else if (countFlags < 0 && countFlags >= -9) {
                secondNum.textContent = '';
                flags.textContent = firstNum.textContent + secondNum.textContent + countFlags
            } else if (countFlags < -9) {
                firstNum.textContent = '';
                flags.textContent = firstNum.textContent + secondNum.textContent + countFlags
            };

        } else if (event.target.getAttribute('class') === 'cell') {
            countFlags++;
            if (countFlags < -9) {
                flags.textContent = firstNum.textContent + secondNum.textContent + countFlags
            } else if (countFlags < 0 && countFlags >= -9) {
                firstNum.textContent = '0';
                flags.textContent = firstNum.textContent + secondNum.textContent + countFlags
            } else if (countFlags < 10 && countFlags >= 0) {
                secondNum.textContent = '0'
                flags.textContent = firstNum.textContent + secondNum.textContent + countFlags
            } else if (countFlags = 10) {
                firstNum.textContent = '0';
                secondNum.textContent = '';
                flags.textContent = firstNum.textContent + secondNum.textContent + countFlags
            };
        }
    });

    /*** Функция на валидность ряда и колонки ***/
    function isValid(row, column) {
        return row >= 0 && row < 8 && column >= 0 && column < 8;
    };

    /*** Функция которая проверяет соседнии ячейки, яввляются ли они минами ***/
    function getCountBomb(row, column) {
        let count = 0;
        for (let i = -1; i <= 1; i++) {
            for (let j = -1; j <= 1; j++) {
                if (isBomb(row + j, column + i)) {
                    count++
                }
            }
        }
        return count;
    };

    /*** Функция для открытия ячеек ***/
    function open(row, column) {
        if (!isValid(row, column)) return;

        const index = row * 8 + column;
        const cell = cells[index];

        if (cell.disabled === true) return;

        cell.disabled = true

        const btn = document.querySelector('.btn');
        const end = document.querySelector('.end');
        const h1 = document.querySelector('h1');
        const h2 = document.querySelector('h2');
        const fog = document.querySelector('.fog__container');

        /*** Функция для открытия всех бомб на поле ***/
        function openAllBombs() {
            cells.forEach((item) => {
                if (item.getAttribute('class') !== 'bombed') {
                    bombs.forEach((bomb) => {
                        if (bomb === cells.indexOf(item)) {
                            item.classList.add('bomb');
                        }
                    })
                }
            })
        };

        /*** Если бомба, тогда конец игры ***/
        if (isBomb(row, column)) {
            cell.classList.remove('cell');
            cell.classList.remove('flaged');
            cell.classList.add('bombed');
            field.removeEventListener('click', openCell);
            field.removeEventListener('contextmenu', setFlag);
            btn.classList.add('btnGameOver');
            openAllBombs();
            let counterOpacity = 0;
            const styleEnd = setInterval(function () {
                if (counterOpacity < 1) {
                    end.style.opacity = counterOpacity;
                    counterOpacity += .1;
                }
                else {
                    clearInterval(styleEnd);
                }
            }, 200);            
            h1.style.display = 'none';            
            h2.style.display = 'inline-block';
            h2.classList.add('blur')
            bangMines();
            fog.style.display = 'block';
            stopTimer();
            return
        };

        /*** Если открыты все ячейки кроме мин, тогда конец игры (победа) ***/
        closedCount--;
        const win = document.querySelector('.win');
        if (closedCount <= 10) {
            btn.classList.add('btnWin');
            let counterFontSize = 0;
            const styleWin = setInterval(function () {
                if (counterFontSize < 46) {
                    counterFontSize += 2;
                    win.style.fontSize = counterFontSize + 'px';
                }
            }, 200);
            const colors = ["#757575", "#616161", "#424242", "#689F38", "#FFEB3B", "#0097A7", "#EF5350"];
            let nextColor = 0;
            setInterval(() => {
                win.style.color = colors[nextColor++ % colors.length];
            }, 200);

            firework.start();
            firework2.start();
            stopTimer();
        };

        const count = getCountBomb(row, column);
        if (count !== 0) {
            cell.textContent = count;

            switch (count) {
                case 1: cell.textContent = cell.classList.add('one');
                    cell.classList.remove('cell')
                    if (cell.classList.contains('flaged')) {
                        cell.classList.remove('flaged')
                    }
                    break;
                case 2: cell.textContent = cell.classList.add('two');
                    cell.classList.remove('cell')
                    if (cell.classList.contains('flaged')) {
                        cell.classList.remove('flaged')
                    }
                    break;
                case 3: cell.textContent = cell.classList.add('three');
                    cell.classList.remove('cell')
                    if (cell.classList.contains('flaged')) {
                        cell.classList.remove('flaged')
                    }
                    break;
                case 4: cell.textContent = cell.classList.add('four');
                    cell.classList.remove('cell')
                    if (cell.classList.contains('flaged')) {
                        cell.classList.remove('flaged')
                    }
                    break;
                case 5: cell.textContent = cell.classList.add('five');
                    cell.classList.remove('cell')
                    if (cell.classList.contains('flaged')) {
                        cell.classList.remove('flaged')
                    }
                    break;
                case 6: cell.textContent = cell.classList.add('six');
                    cell.classList.remove('cell')
                    if (cell.classList.contains('flaged')) {
                        cell.classList.remove('flaged')
                    }
                    break;
                case 7: cell.textContent = cell.classList.add('seven');
                    cell.classList.remove('cell')
                    if (cell.classList.contains('flaged')) {
                        cell.classList.remove('flaged')
                    }
                    break;
                case 8: cell.textContent = cell.classList.add('eight');
                    cell.classList.remove('cell')
                    if (cell.classList.contains('flaged')) {
                        cell.classList.remove('flaged')
                    }
                    break;
            }
            return
        }

        for (let i = -1; i <= 1; i++) {
            for (let j = -1; j <= 1; j++) {
                open(row + j, column + i);
            }
        }
        cell.classList.remove('cell');
        cell.classList.remove('flaged')
        return cell.classList.add('zero');
    };

    /*** Функция для определения является ли открываемая ячейка бомбой ***/
    function isBomb(row, column) {
        if (!isValid(row, column)) return false;

        const index = row * 8 + column;
        return bombs.includes(index);
    };

    /*** Кнопка запуска игры заново ***/
    const button = document.querySelector('input');
    button.addEventListener('click', () => {
        window.location.reload();
    });

    /*** Код для анимации эффекта салюта ***/
    const firework = JS_FIREWORKS.Fireworks({
        id: 'fireworks-canvas',
        hue: 120,
        particleCount: 50,
        delay: 0,
        minDelay: 20,
        maxDelay: 40,
        boundaries: {
            top: 50,
            bottom: 240,
            left: 50,
            right: 590
        },
        fireworkSpeed: 2,
        fireworkAcceleration: 1.05,
        particleFriction: .95,
        particleGravity: 1.5
    });
    const firework2 = JS_FIREWORKS.Fireworks({
        id: 'fireworks-canvas2',
        hue: 120,
        particleCount: 50,
        delay: 0,
        minDelay: 20,
        maxDelay: 40,
        boundaries: {
            top: 50,
            bottom: 240,
            left: 50,
            right: 590
        },
        fireworkSpeed: 2,
        fireworkAcceleration: 1.05,
        particleFriction: .95,
        particleGravity: 1.5
    });

    /*** Код для анимации заголовка "Mineswipper" ***/
    const mines = document.querySelectorAll('h2 .mines');
    function bangMines() {
        mines.forEach((item) => {
            let bottomRan = (Math.floor(Math.random() * 820) - 710);
            let rightRan = (Math.floor(Math.random() * 1050) - 650);
            let transformRan = (Math.floor(Math.random() * 360));
            let sizeRan = (Math.floor(Math.random() * 5) + 50);

            item.style.bottom = `${bottomRan}px`;
            item.style.right = `${rightRan}px`;
            item.style.transform = `rotate(${transformRan}deg)`;
            item.style.fontSize = `${sizeRan}px`;

        })
    }  
};






