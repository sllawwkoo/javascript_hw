/* Разработайте функцию-конструктор, которая будет создавать объект Human(человек). 
Создайте массив объектов и реализуйте функцию, которая будет сортировать элементы массива
 по значению свойства Age по возрастанию или по убыванию.
 */

 //Создаем конструктор Human
function Human(name, surname, nationality, age) {
    this.name = name,
        this.surname = surname,
        this.nationality = nationality,
        this.age = age
}

//Создаем 5 объектов на основании конструктора Human
const john = new Human('John', 'Smith', 'american', 26);
const thomas = new Human('Thomas', 'Muller', 'german', 77);
const simone = new Human('Simone', 'Inzaghi', 'italian', 43);
const pablo = new Human('Pablo', 'Garcia', 'hispanic', 7);
const nils = new Human('Nils', 'Jonsson', 'swede', 38);

//Создаём массив объектов
const guys = [john, thomas, simone, pablo, nils];

//Функция которая выводит "ключ - значение" объектов которые находятся в массиве
function showArray(array) {
    array.forEach(item => {
        document.write(`${item.name} ${item.surname} ${item.nationality} ${item.age}<br>`);
    });
}

//Функция сортировки значения ключа age по возрастанию
function sortByAgeUp(array) {
    array.sort(function (a, b) {
        return a.age - b.age;
    });
}
//Функция сортировки значения ключа age по убыванию
function sortByAgeUDown(array) {
    array.sort(function (a, b) {
        return b.age - a.age;
    });
}

document.write('Выводим созданный массиив "guys": <br><br>');
showArray(guys);

document.write('<br><hr><br> Выводим отсортированый массиив "guys" по возрастанию возраста : <br><br>');
sortByAgeUp(guys);
showArray(guys);

document.write('<br><hr><br> Выводим отсортированый массиив "guys" по убыванию возраста : <br><br>');
sortByAgeUDown(guys);
showArray(guys);