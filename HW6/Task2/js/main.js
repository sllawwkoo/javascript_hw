/* Разработайте функцию-конструктор, которая будет создавать объект Human (человек), 
добавьте на свое усмотрение свойства и методы в этот объект.
 Подумайте, какие методы и свойства следует сделать уровня экземпляра, какие уровня функции-конструктора. */

function Human(name, surname, nationality, age) {
    this.name = name,
        this.surname = surname,
        this.nationality = nationality,
        this.age = age
}
//Метод экземпляра
Human.prototype.showGuy = function () {
    document.write(`${this.name} ${this.surname} ${this.nationality} ${this.age}<br>`);
}
//Метод экземпляра
Human.prototype.say = function () {
    document.write(`Hello! My name is ${this.name} ${this.surname} <br>`);
}

//Свойство конструктора
Human.lives = 'lives on the Earth';

//Метод конструктора, который выводит содержимое объекта (ключ - значение)
Human.getKeyAndValua = function (obj) {
    for (let prop in obj) {
        if (typeof (obj[prop] === 'object')) {
            document.write(`Ключ: ${prop} значение: ${obj[prop]} <br>`)
        }
    }
};

//Создаем 5 объектов на основании конструктора Human
const john = new Human('John', 'Smith', 'american', 26);
const thomas = new Human('Thomas', 'Muller', 'german', 77);
const simone = new Human('Simone', 'Inzaghi', 'italian', 43);
const pablo = new Human('Pablo', 'Garcia', 'hispanic', 7);
const nils = new Human('Nils', 'Jonsson', 'swede', 38);

simone.say();
john.say();
document.write('<br><hr><br>');

pablo.showGuy();
thomas.showGuy();
document.write('<br><hr><br>');

Human.getKeyAndValua(nils);