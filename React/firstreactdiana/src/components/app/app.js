import React from 'react';
import Tableheader from '../tableheader/tableheader'
import Tablebody from "../tablebody/tablebody";

const App = () => {
    return (
        <table>
            <Tableheader></Tableheader>
            <Tablebody></Tablebody>
        </table>
    )
}

export default App; 