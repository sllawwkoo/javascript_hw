import React from 'react'
import data from '../../data/data'

console.log(Array.isArray(data.exchangeRate) )
const Tablebody = () => {
    return (
        <tbody>
        {
        data.exchangeRate.map((value) => {
        return <tr>
            <td>{value.currency}</td>
            <td>{value.saleRateNB.toFixed(2)}</td>
            <td>{value.purchaseRateNB.toFixed(2)}</td>
        </tr>
        }) 
        }
        </tbody>
    )
}

export default Tablebody