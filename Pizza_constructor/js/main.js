/* Создать сайт по заказу пиццы используя семантическую верстку. 
(Пример "https://dominos.ua/ru/kyiv/create/" ) 
На сайте должна быть возможность сделать пиццу самому. 
При создании пиццы пользователь выбирает ингредиенты и размер пиццы. ( маленькая, средняя или большая). 
Ингредиенты ( сыр, пепперони, перец, кукуруза, ананасы и т.д) и также 3 вида соуса. 
При создании пиццы цена увеличивается. Все ингредиенты и коржи должны быть в виде картинок и накладываются друг на друга
 ( https://ru.wikipedia.org/wiki/Drag-and-drop ) при выборе в конце принять от пользователя телефон, email, имя 
 и проверить на правильность введения. После чего весь результат заказа отправить на почту philipsevene@gmail.com.
Так же создайте банер получить скидку на пиццу 30% и сделайте чтобы банер от курсора пользователя убегал. */

const buttonSize = document.querySelector("#pizza");
const price = document.querySelector(".price > p:nth-child(2)");
const sauces = document.querySelector('.sauces > p:nth-child(2)');
const topings = document.querySelector('.topings > p:nth-child(2)')

/* wrappers count switcher */
const wrapper1 = document.querySelector('.wrapper1');
const wrapper2 = document.querySelector('.wrapper2');
const wrapper3 = document.querySelector('.wrapper3');
const wrapper4 = document.querySelector('.wrapper4');
const wrapper5 = document.querySelector('.wrapper5');
const wrapper6 = document.querySelector('.wrapper6');
const wrapper7 = document.querySelector('.wrapper7');
const wrapper8 = document.querySelector('.wrapper8');
const wrapper9 = document.querySelector('.wrapper9');

/* counter values */
const counterValue1 = document.querySelector('#count1');
const counterValue2 = document.querySelector('#count2');
const counterValue3 = document.querySelector('#count3');
const counterValue4 = document.querySelector('#count4');
const counterValue5 = document.querySelector('#count5');
const counterValue6 = document.querySelector('#count6');
const counterValue7 = document.querySelector('#count7');
const counterValue8 = document.querySelector('#count8');
const counterValue9 = document.querySelector('#count9');

const pizza = {
  priceCake: 70,
  sauseClassicName: [],
  sauseBbqName: [],
  sauseRikottaName: [],
  toppingCheeseName: [],
  toppingFetaName: [],
  toppingMozzarellaName: [],
  toppingVealName: [],
  toppingTomatoName: [],
  toppingMushroomName: [],

  // методы для записи названий соусов
  outputSauseClassicName() {
    let printSauseClassicName = [];
    let amount = 0;
    this.sauseClassicName.forEach(function (element) {
      let valueAmount = amount;
      amount = ++valueAmount;
      if (printSauseClassicName.length === element.count) {
        let valueCount = parseInt(element.count);
        element.count = ++valueCount
        printSauseClassicName.unshift(`${element.name} ${amount} шт.`);
      };
      if (printSauseClassicName.length < element.count) {
        let valueCount = parseInt(element.count);
        element.count = --valueCount;
        printSauseClassicName.unshift(`${element.name} ${amount} шт.`);
      }
    });
    return printSauseClassicName[0];
  },

  outputSauseBbqName() {
    let printSauseBbqName = [];
    let amount = 0;
    this.sauseBbqName.forEach(function (element) {
      let valueAmount = amount;
      amount = ++valueAmount;
      if (printSauseBbqName.length === element.count) {
        let valueCount = parseInt(element.count);
        element.count = ++valueCount;
        printSauseBbqName.unshift(`${element.name} ${amount} шт.`);
      };
      if (printSauseBbqName.length < element.count) {
        let valueCount = parseInt(element.count);
        element.count = --valueCount;
        printSauseBbqName.unshift(`${element.name} ${amount} шт.`);
      }
    })
    return printSauseBbqName[0];
  },

  outputSauseRikottaName() {
    let printSauseRikottaName = [];
    let amount = 0;
    this.sauseRikottaName.forEach(function (element) {
      let valueAmount = amount;
      amount = ++valueAmount;
      if (printSauseRikottaName.length === element.count) {
        let valueCount = parseInt(element.count);
        element.count = ++valueCount;
        printSauseRikottaName.unshift(`${element.name} ${amount} шт.`);
      };
      if (printSauseRikottaName.length < element.count) {
        let valueCount = parseInt(element.count);
        element.count = --valueCount;
        printSauseRikottaName.unshift(`${element.name} ${amount} шт.`);
      }
    })
    return printSauseRikottaName[0];
  },

  outputFullNameSauce() {
    return this.outputSauseClassicName() + "  " + this.outputSauseBbqName() + "  " + this.outputSauseRikottaName() + "  "
  },
  /*************************************************************************************/

  //метод для записи названий топингов
  outputToppingCheeseName() {
    let printToppingCheeseName = [];
    let amount = 0;
    this.toppingCheeseName.forEach(function (element) {
      let valueAmount = amount;
      amount = ++valueAmount;
      if (printToppingCheeseName.length === element.count) {
        let valueCount = parseInt(element.count);
        element.count = ++valueCount;
        printToppingCheeseName.unshift(`${element.name} ${amount} шт.`);
      };
      if (printToppingCheeseName.length < element.count) {
        let valueCount = parseInt(element.count);
        element.count = --valueCount;
        printToppingCheeseName.unshift(`${element.name} ${amount} шт.`);
      }
    })
    return printToppingCheeseName[0];
  },

  outputToppingFetaName() {
    let printToppingFetaName = [];
    let amount = 0;
    this.toppingFetaName.forEach(function (element) {
      let valueAmount = amount;
      amount = ++valueAmount;
      if (printToppingFetaName.length === element.count) {
        let valueCount = parseInt(element.count);
        element.count = ++valueCount;
        printToppingFetaName.unshift(`${element.name} ${amount} шт.`);
      };
      if (printToppingFetaName.length < element.count) {
        let valueCount = parseInt(element.count);
        element.count = --valueCount;
        printToppingFetaName.unshift(`${element.name} ${amount} шт.`);
      }
    })
    return printToppingFetaName[0];
  },

  outputToppingMozzarellaName() {
    let printToppingMozzarellaName = [];
    let amount = 0;
    this.toppingMozzarellaName.forEach(function (element) {
      let valueAmount = amount;
      amount = ++valueAmount;
      if (printToppingMozzarellaName.length === element.count) {
        let valueCount = parseInt(element.count);
        element.count = ++valueCount;
        printToppingMozzarellaName.unshift(`${element.name} ${amount} шт.`);
      };
      if (printToppingMozzarellaName.length < element.count) {
        let valueCount = parseInt(element.count);
        element.count = --valueCount;
        printToppingMozzarellaName.unshift(`${element.name} ${amount} шт.`);
      }
    })
    return printToppingMozzarellaName[0];
  },

  outputToppingVealName() {
    let printToppingVealName = [];
    let amount = 0;
    this.toppingVealName.forEach(function (element) {
      let valueAmount = amount;
      amount = ++valueAmount;
      if (printToppingVealName.length === element.count) {
        let valueCount = parseInt(element.count);
        element.count = ++valueCount;
        printToppingVealName.unshift(`${element.name} ${amount} шт.`);
      };
      if (printToppingVealName.length < element.count) {
        let valueCount = parseInt(element.count);
        element.count = --valueCount;
        printToppingVealName.unshift(`${element.name} ${amount} шт.`);
      }
    })
    return printToppingVealName[0];
  },

  outputToppingTomatoName() {
    let printToppingTomatoName = [];
    let amount = 0;
    this.toppingTomatoName.forEach(function (element) {
      let valueAmount = amount;
      amount = ++valueAmount;
      if (printToppingTomatoName.length === element.count) {
        let valueCount = parseInt(element.count);
        element.count = ++valueCount;
        printToppingTomatoName.unshift(`${element.name} ${amount} шт.`);
      };
      if (printToppingTomatoName.length < element.count) {
        let valueCount = parseInt(element.count);
        element.count = --valueCount;
        printToppingTomatoName.unshift(`${element.name} ${amount} шт.`);
      }
    })
    return printToppingTomatoName[0];
  },

  outputToppingMushroomName() {
    let printToppingMushroomName = [];
    let amount = 0;
    this.toppingMushroomName.forEach(function (element) {
      let valueAmount = amount;
      amount = ++valueAmount;
      if (printToppingMushroomName.length === element.count) {
        let valueCount = parseInt(element.count);
        element.count = ++valueCount;
        printToppingMushroomName.unshift(`${element.name} ${amount} шт.`);
      };
      if (printToppingMushroomName.length < element.count) {
        let valueCount = parseInt(element.count);
        element.count = --valueCount;
        printToppingMushroomName.unshift(`${element.name} ${amount} шт.`);
      }
    })
    return printToppingMushroomName[0];
  },




  outputFullNameTopping() {
    return this.outputToppingCheeseName() + "  " + this.outputToppingFetaName() + "  " + this.outputToppingMozzarellaName() + "  "
      + this.outputToppingVealName() + "  " + this.outputToppingTomatoName() + "  " + this.outputToppingMushroomName() + "  ";
  },
  /******************************************************/

  //методы для вычисления цены соусов
  priceSauseClassic() {
    let sumSauseClassicPrice = 0;
    this.sauseClassicName.forEach((element) => {
      sumSauseClassicPrice += element.price
    });
    return sumSauseClassicPrice;
  },
  priceSauseBBQ() {
    let sumSauseBbqPrice = 0;
    this.sauseBbqName.forEach((element) => {
      sumSauseBbqPrice += element.price
    });
    return sumSauseBbqPrice;
  },
  priceSauseRikotta() {
    let sumSauseRikottaPrice = 0;
    this.sauseRikottaName.forEach((element) => {
      sumSauseRikottaPrice += element.price
    });
    return sumSauseRikottaPrice;
  },
  //  расчитываем общую стоимость всех соусов
  priceSause() {
    return this.priceSauseClassic() + this.priceSauseBBQ() + this.priceSauseRikotta()
  },
  /********************************************************************/


  //  методы для вычисления цены топинга
  priceToppingCheese() {
    let sumToppingCheesePrice = 0;
    this.toppingCheeseName.forEach((element) => {
      sumToppingCheesePrice += element.price
    });
    return sumToppingCheesePrice;
  },
  priceToppingFeta() {
    let sumToppingFetaPrice = 0;
    this.toppingFetaName.forEach((element) => {
      sumToppingFetaPrice += element.price
    });
    return sumToppingFetaPrice;
  },
  priceToppingMozzarella() {
    let sumToppingMozzarellaPrice = 0;
    this.toppingMozzarellaName.forEach((element) => {
      sumToppingMozzarellaPrice += element.price
    });
    return sumToppingMozzarellaPrice;
  },
  priceToppingVeal() {
    let sumToppingVealPrice = 0;
    this.toppingVealName.forEach((element) => {
      sumToppingVealPrice += element.price
    });
    return sumToppingVealPrice;
  },
  priceToppingTomato() {
    let sumToppingTomatoPrice = 0;
    this.toppingTomatoName.forEach((element) => {
      sumToppingTomatoPrice += element.price
    });
    return sumToppingTomatoPrice;
  },
  priceToppingMushroom() {
    let sumToppingMushroomPrice = 0;
    this.toppingMushroomName.forEach((element) => {
      sumToppingMushroomPrice += element.price
    });
    return sumToppingMushroomPrice;
  },

  //  расчитываем общую стоимость всех топингов
  priceTopping() {
    return this.priceToppingCheese() + this.priceToppingFeta() + this.priceToppingMozzarella()
      + this.priceToppingVeal() + this.priceToppingTomato() + this.priceToppingMushroom();
  },
  /*************************************************************************************/

  // общая стоимость пиццы
  getFullPricePizza() {
    return this.priceCake + this.priceSause() + this.priceTopping() + " грн.";
  },
};

/* choose size pizza */
buttonSize.addEventListener("click", (e) => {
  if (e.target.value === "big") {
    pizza.priceCake = 70;
  } else if (e.target.value === "mid") {
    pizza.priceCake = 50;
  } else if (e.target.value === "small") {
    pizza.priceCake = 35;
  }
  showPizzaInfo(pizza);
});

//Отображаем цену
const showPizzaInfo = (obj) => {
  price.textContent = obj.getFullPricePizza();

  sauces.innerHTML = obj.outputFullNameSauce();
  const sauces_string = sauces.textContent;
  const outputSauces = sauces_string.replace(/<([^>]+)>/g, "&lt$1&gt").replace(/((\S+\s){3})/g, "$1<br>");
  sauces.innerHTML = outputSauces.split('undefined').join('');

  topings.innerHTML = obj.outputFullNameTopping();
  const topings_string = topings.textContent;
  const outputTopings = topings_string.replace(/<([^>]+)>/g, "&lt$1&gt").replace(/((\S+\s){3})/g, "$1<br>");
  topings.innerHTML = outputTopings.split('undefined').join('');
};

showPizzaInfo(pizza);

/*************Drag and Drop***********/

const dragIngridients = document.querySelectorAll('.draggable');
const dropTable = document.querySelector('.table');

let draggedIngridient = null;

dragIngridients.forEach((dragIngridient) => {
  dragIngridient.addEventListener('dragstart', handlerDragstart);
  dragIngridient.addEventListener('dragend', handlerDragend);
});

dropTable.addEventListener('dragenter', handlerDragenter);
dropTable.addEventListener('dragleave', handlerDragleave);
dropTable.addEventListener('dragover', handlerDragover);
dropTable.addEventListener('drop', handlerDrop);

function handlerDragstart() {
  this.classList.add('draggable--active');
  draggedIngridient = this;
}

function handlerDragend() {
  this.classList.remove('draggable--active');
  draggedIngridient = null;
}

function handlerDragenter(event) {
  event.preventDefault();
  this.classList.add('table--active');
}

function handlerDragleave() {
  this.classList.remove('table--active');
}

function handlerDragover(event) {
  event.preventDefault();
}

function handlerDrop(event) {
  event.preventDefault();
  const clone = draggedIngridient.cloneNode(true);
  this.append(clone);
  clone.classList.remove('draggable--active');
  dropTable.classList.remove('table--active');
  if (clone.getAttribute('id') === 'sauceClassic') {
    wrapper1.classList.add('wrapperVisible');
    clone.removeAttribute('id');
    clone.setAttribute('class', 'sauceClassic2');
    let count = parseInt(counterValue1.value);
    counterValue1.value = ++count;
    pizza.sauseClassicName.unshift({ name: `Класичний`, price: 15, count: 0, });
  };
  if (clone.getAttribute('id') === 'sauceBBQ') {
    wrapper2.classList.add('wrapperVisible');
    clone.removeAttribute('id');
    clone.setAttribute('class', 'sauceBBQ2');
    let count = parseInt(counterValue2.value);
    counterValue2.value = ++count;
    pizza.sauseBbqName.unshift({ name: `Барбекю`, price: 17, count: 0, });
  };
  if (clone.getAttribute('id') === 'sauceRikotta') {
    wrapper3.classList.add('wrapperVisible');
    clone.removeAttribute('id');
    clone.setAttribute('class', 'sauceRikotta2');
    let count = parseInt(counterValue3.value);
    counterValue3.value = ++count;
    pizza.sauseRikottaName.unshift({ name: `Рікотта`, price: 19, count: 0, });
  };
  if (clone.getAttribute('id') === 'moc1') {
    wrapper4.classList.add('wrapperVisible');
    clone.removeAttribute('id');
    clone.setAttribute('class', 'moc-1');
    let count = parseInt(counterValue4.value);
    counterValue4.value = ++count;
    pizza.toppingCheeseName.unshift({ name: `Сир_звичайний`, price: 22, count: 0, });
  };
  if (clone.getAttribute('id') === 'moc2') {
    wrapper5.classList.add('wrapperVisible');
    clone.removeAttribute('id');
    clone.setAttribute('class', 'moc-2');
    let count = parseInt(counterValue5.value);
    counterValue5.value = ++count;
    pizza.toppingFetaName.unshift({ name: `Сир_фета`, price: 24, count: 0, });
  };
  if (clone.getAttribute('id') === 'moc3') {
    wrapper6.classList.add('wrapperVisible');
    clone.removeAttribute('id');
    clone.setAttribute('class', 'moc-3');
    let count = parseInt(counterValue6.value);
    counterValue6.value = ++count;
    pizza.toppingMozzarellaName.unshift({ name: `Моцарелла`, price: 26, count: 0, });
  };
  if (clone.getAttribute('id') === 'telya') {
    wrapper7.classList.add('wrapperVisible');
    clone.removeAttribute('id');
    clone.setAttribute('class', 'telya2');
    let count = parseInt(counterValue7.value);
    counterValue7.value = ++count;
    pizza.toppingVealName.unshift({ name: `Телятина`, price: 30, count: 0, });
  };
  if (clone.getAttribute('id') === 'vetch1') {
    wrapper8.classList.add('wrapperVisible');
    clone.removeAttribute('id');
    clone.setAttribute('class', 'vetch-1');
    let count = parseInt(counterValue8.value);
    counterValue8.value = ++count;
    pizza.toppingTomatoName.unshift({ name: `Помідори`, price: 20, count: 0, });
  };
  if (clone.getAttribute('id') === 'vetch2') {
    wrapper9.classList.add('wrapperVisible');
    clone.removeAttribute('id');
    clone.setAttribute('class', 'vetch-2');
    let count = parseInt(counterValue9.value);
    counterValue9.value = ++count;
    pizza.toppingMushroomName.unshift({ name: `Гриби`, price: 25, count: 0, });
  };
  showPizzaInfo(pizza)
}



/*************END Drag and Drop***********/

/** Click count switcher and choose your toping and sause **/

const btnMinus = document.querySelectorAll('.minus');
btnMinus.forEach((buttonItem) => {
  buttonItem.addEventListener('click', () => {

    let countIngr1 = parseInt(counterValue1.value);
    if (countIngr1 >= 1 && buttonItem.getAttribute('id') === 'minus1') {
      counterValue1.value = --countIngr1;
      pizza.sauseClassicName.shift()
    };
    if (countIngr1 < 1 && buttonItem.getAttribute('id') === 'minus1') {
      wrapper1.classList.remove('wrapperVisible');
      const sauceClassic = document.querySelectorAll('.sauceClassic2');
      sauceClassic.forEach((element) => {
        element.remove();
      });
      pizza.sauseClassicName.splice(0)
    };
    let countIngr2 = parseInt(counterValue2.value);
    if (countIngr2 >= 1 && buttonItem.getAttribute('id') === 'minus2') {
      counterValue2.value = --countIngr2;
      pizza.sauseBbqName.shift();
    };
    if (countIngr2 < 1 && buttonItem.getAttribute('id') === 'minus2') {
      wrapper2.classList.remove('wrapperVisible');
      const sauceBBQ = document.querySelectorAll('.sauceBBQ2');
      sauceBBQ.forEach((element) => {
        element.remove();
      });
      pizza.sauseBbqName.splice(0)
    };
    let countIngr3 = parseInt(counterValue3.value);
    if (countIngr3 >= 1 && buttonItem.getAttribute('id') === 'minus3') {
      counterValue3.value = --countIngr3;
      pizza.sauseRikottaName.shift();
    }
    if (countIngr3 < 1 && buttonItem.getAttribute('id') === 'minus3') {
      wrapper3.classList.remove('wrapperVisible');
      const sauceRikotta = document.querySelectorAll('.sauceRikotta2');
      sauceRikotta.forEach((element) => {
        element.remove();
      });
      pizza.sauseRikottaName.splice(0)
    };
    let countIngr4 = parseInt(counterValue4.value);
    if (countIngr4 >= 1 && buttonItem.getAttribute('id') === 'minus4') {
      counterValue4.value = --countIngr4;
      pizza.toppingCheeseName.shift();
    }
    if (countIngr4 < 1 && buttonItem.getAttribute('id') === 'minus4') {
      wrapper4.classList.remove('wrapperVisible');
      const cheese = document.querySelectorAll('.moc-1');
      cheese.forEach((element) => {
        element.remove();
      });
      pizza.toppingCheeseName.splice(0)
    };
    let countIngr5 = parseInt(counterValue5.value);
    if (countIngr5 >= 1 && buttonItem.getAttribute('id') === 'minus5') {
      counterValue5.value = --countIngr5;
      pizza.toppingFetaName.shift();
    }
    if (countIngr5 < 1 && buttonItem.getAttribute('id') === 'minus5') {
      wrapper5.classList.remove('wrapperVisible');
      const feta = document.querySelectorAll('.moc-2');
      feta.forEach((element) => {
        element.remove();
      });
      pizza.toppingFetaName.splice(0)
    };
    let countIngr6 = parseInt(counterValue6.value);
    if (countIngr6 >= 1 && buttonItem.getAttribute('id') === 'minus6') {
      counterValue6.value = --countIngr6;
      pizza.toppingMozzarellaName.shift();
    }
    if (countIngr6 < 1 && buttonItem.getAttribute('id') === 'minus6') {
      wrapper6.classList.remove('wrapperVisible');
      const mozzarella = document.querySelectorAll('.moc-3');
      mozzarella.forEach((element) => {
        element.remove();
      });
      pizza.toppingMozzarellaName.splice(0)
    };
    let countIngr7 = parseInt(counterValue7.value);
    if (countIngr7 >= 1 && buttonItem.getAttribute('id') === 'minus7') {
      counterValue7.value = --countIngr7;
      pizza.toppingVealName.shift();
    }
    if (countIngr7 < 1 && buttonItem.getAttribute('id') === 'minus7') {
      wrapper7.classList.remove('wrapperVisible');
      const veal = document.querySelectorAll('.telya2');
      veal.forEach((element) => {
        element.remove();
      });
      pizza.toppingVealName.splice(0)
    };
    let countIngr8 = parseInt(counterValue8.value);
    if (countIngr8 >= 1 && buttonItem.getAttribute('id') === 'minus8') {
      counterValue8.value = --countIngr8;
      pizza.toppingTomatoName.shift();
    }
    if (countIngr8 < 1 && buttonItem.getAttribute('id') === 'minus8') {
      wrapper8.classList.remove('wrapperVisible');
      const tomato = document.querySelectorAll('.vetch-1');
      tomato.forEach((element) => {
        element.remove();
      });
      pizza.toppingTomatoName.splice(0)
    };
    let countIngr9 = parseInt(counterValue9.value);
    if (countIngr9 >= 1 && buttonItem.getAttribute('id') === 'minus9') {
      counterValue9.value = --countIngr9;
      pizza.toppingMushroomName.shift();
    }
    if (countIngr9 < 1 && buttonItem.getAttribute('id') === 'minus9') {
      wrapper9.classList.remove('wrapperVisible');
      const mushroom = document.querySelectorAll('.vetch-2');
      mushroom.forEach((element) => {
        element.remove();
      });
      pizza.toppingMushroomName.splice(0)
    };

    showPizzaInfo(pizza)
  })
})
/****************************************************************************/

/*** banner move ***/

const banner = document.querySelector('#banner');
banner.addEventListener('mousemove', () => {
  let bottomRan = (Math.floor(Math.random() * 86));
  let rightRan = (Math.floor(Math.random() * 86));

  banner.style.bottom = bottomRan + "%";
  banner.style.right = rightRan + "%"
})
/****************************************************************************/

/*** отправка заказа и валидация ***/
const buttonOrder = document.querySelector('#button');
const inputName = document.getElementsByName('name')[0];
const inputPhone = document.getElementsByName('phone')[0];
const inputEmail = document.getElementsByName('email')[0];
const span1 = document.querySelector('#span1');
const span2 = document.querySelector('#span2');
const span3 = document.querySelector('#span3');

buttonOrder.addEventListener('click', () => {
  const patternName = /[А-Я]{1}[а-я]+/;
  const patternPhone = /\+380\d{9}/;
  const patternEmail = /\b[a-z0-9._]+@[a-z0-9.-]+\.[a-z]{2,4}\b/i;

  function isValidName() {
    if (!patternName.test(inputName.value)) {
      span1.style.visibility = 'visible'
    } else return true
  };
  isValidName();
  function isValidPhone() {
    if (!patternPhone.test(inputPhone.value)) {
      span2.style.visibility = 'visible'
    } else return true
  };
  isValidPhone();
  function isValidEmail() {
    if (!patternEmail.test(inputEmail.value)) {
      span3.style.visibility = 'visible'
    } else return true
  };
  isValidEmail();
  if ((isValidName() && isValidPhone() && isValidEmail()) === true) {
    document.location = './thank-you.html'
  }
   console.log(inputEmail.value)
});

inputName.addEventListener('click',() => {
  if (span1.style.visibility === 'visible') {
    span1.style.visibility = 'hidden';
  }
});
inputPhone.addEventListener('click',() => {
  if (span2.style.visibility === 'visible') {
    span2.style.visibility = 'hidden';
  }
});
inputEmail.addEventListener('click',() => {
  if (span3.style.visibility === 'visible') {
    span3.style.visibility = 'hidden';
  }
});
