//Создать объект "Документ", в котором определить свойства "Заголовок, тело, футер, дата". 
//Создать в объекте вложенный объект - "Приложение". 
//Создать в объекте "Приложение", вложенные объекты, "Заголовок, тело, футер, дата". 
//Создать методы для заполнения и отображения документа. 

const doc = {
    title: " ",
    body: " ",
    footer: " ",
    date: " ",
    writeInDoc: function () {
        this.title = prompt("Введите название документа: ");
        this.body = prompt("Заполните документ: ");
        this.footer = prompt("Введите ваши данные: ");
        this.date = prompt("Введите текущюю дату")
    },
    app: {
        titleApp: " ",
        bodyApp: " ",
        footerApp: " ",
        dateApp: " ",
        writeInApp: function () {
            doc.app.titleApp = prompt("Введите название приложения: ");
            doc.app.bodyApp = prompt("Опишите приложение: ");
            doc.app.footerApp = prompt("Введите ваши данные: ");
            doc.app.dateApp = prompt("Введите текущюю дату")
        }
    },
    getKeyAndValua: function (obj) {
        for (let prop in obj) {
            if (typeof (obj[prop] === 'object')) {
                document.write(`Ключ: ${prop} значение: ${obj[prop]} <br>`)
            } 
            for (let prop2 in obj[prop]) {
                if (typeof (obj[prop][prop2]) === 'object'){
                    document.write(obj[prop][prop2])
                }
            }
        }
    }    
};
document.write("<p>Заполняем содержимое значений ключей объекта 'Документ': <br><br>");
doc.writeInDoc();

document.write("<hr><p>Заполняем содержимое значений ключей вложеного объекта 'Приложение': <br><br>");
doc.app.writeInApp();

document.write("<hr><p>Выводим на экран содержимое объект 'Документ': <br><br>");
doc.getKeyAndValua(doc);

document.write("<hr><p>Выводим на экран содержимое вложеного объекта 'Приложение': <br><br>");
doc.getKeyAndValua(doc.app)


