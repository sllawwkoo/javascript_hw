/* Написать функцию createNewUser(), которая будет создавать и возвращать объект newUser.
При вызове функция должна спросить у вызывающего имя и фамилию.
Используя данные, введенные пользователем, создать объект newUser со свойствами firstName и lastName.
Добавить в объект newUser метод getLogin(), который будет возвращать первую букву имени пользователя, 
соединенную с фамилией пользователя, все в нижнем регистре (например, Ivan Kravchenko → ikravchenko).
Создать пользователя с помощью функции createNewUser(). Вызвать у пользователя функцию getLogin(). 
Вывести в консоль результат выполнения функции.
________________________________________________________________________________________________________
Дополнить функцию createNewUser() методами подсчета возраста пользователя и его паролем.

Возьмите выполненное задание выше (созданная вами функция createNewUser()) и дополните ее следующим функционалом:

При вызове функция должна спросить у вызывающего дату рождения (текст в формате dd.mm.yyyy) и сохранить ее в поле birthday.
Создать метод getAge() который будет возвращать сколько пользователю лет.
Создать метод getPassword(), который будет возвращать первую букву имени пользователя в верхнем регистре, 
соединенную с фамилией (в нижнем регистре) и годом рождения. (например, Ivan Kravchenko 13.03.1992 → Ikravchenko1992).

Вывести в консоль результат работы функции createNewUser(), а также функций getAge() и getPassword() созданного объекта.
*/


function CreateNewUser() {
    this.firstName = prompt('Введите ваше имя: ', '');
    this.lastName = prompt('Введите вашу фамилию: ', '');
    this.birthday = prompt('Введите вашу дату рождения в формате dd.mm.yyyy:', '')

};
CreateNewUser.prototype.getLogin = function () {
    let newLogin = this.firstName.charAt(0).toLowerCase() + this.lastName.toLowerCase();
    return newLogin;
};
CreateNewUser.prototype.getAge = function () {
    return new Date().getFullYear() - Number(this.birthday.substr(6, 10));
};
CreateNewUser.prototype.getPassword = function () {
    return this.firstName.charAt(0).toLocaleUpperCase() + this.lastName.toLocaleLowerCase() + this.birthday.substr(6, 10);
};

const newUser = new CreateNewUser();
alert(`Ваш логин: ${newUser.getLogin()}`);
alert(`Ваш пароль: ${newUser.getPassword()}`);
alert(`Ваш возраст: ${newUser.getAge()}`);


