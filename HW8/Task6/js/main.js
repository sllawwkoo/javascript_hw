/* Нарисовать на странице круг используя параметры, которые введет пользователь.
При загрузке страницы - показать на ней кнопку с текстом "Нарисовать круг". 
Данная кнопка должна являться единственным контентом в теле HTML документа, 
весь остальной контент должен быть создан и добавлен на страницу с помощью Javascript
При нажатии на кнопку "Нарисовать круг" показывать одно поле ввода - диаметр круга. 
При нажатии на кнопку "Нарисовать" создать на странице 100 кругов (10*10) случайного цвета. 
При клике на конкретный круг - этот круг должен исчезать, при этом пустое место заполняться,
 то есть все остальные круги сдвигаются влево. */

 const inputBtn = document.getElementById('btn').addEventListener('click', () => {
    const diameter = prompt('Введите диаметро круга от 20 до 30: ', '30');
    if (diameter >= 20 && diameter <= 30) {
        for (let i = 1; i <= 100; i++) {
            const circle = document.createElement('div');
            document.querySelector('body > div').append(circle);
            circle.style.width = diameter + 'px';
            circle.style.height = diameter + 'px';
            circle.classList.add('circle');

            let color = 0;
            setInterval(() => {
                color = color + 1 % 360;
                circle.style.backgroundColor = '#' + (Math.random().toString(16) + '000000').substring(2, 8).toUpperCase()
            }, 200);

        }
    } else {
        alert('Вы ввели не правильный диаметр круга!!!');
    }
   
   const divs = document.querySelectorAll('.divs div');
   divs.forEach(circle => circle.addEventListener('click', removeCircle));

   function removeCircle() {
       const circle = this;
       circle.style.opacity = 1;
       const circleId = setInterval(function () {
           if (circle.style.opacity > 0)
               circle.style.opacity -= .1;
           else {
               clearInterval(circleId);
               circle.style.display = 'none';           
           }
       }, 70)
       
    }
    
 });
