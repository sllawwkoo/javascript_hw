/* Создает на странице 10 парахрафов и сделайте так чтобы при нажатии на параграф он исчезал */

for (let i = 10; i >= 1; i--) {
  const p1 = document.createElement("p");
  p1.textContent = "Paragraph " + [i];
  document.body.prepend(p1);
  p1.classList.add("p")
};

const paragraph = document.querySelectorAll('p');

paragraph.forEach(p => p.addEventListener('click', removeParagraph));

function removeParagraph() {
  const p = this;
  p.style.opacity = 1;
  const p2 = setInterval(function () {
    if (p.style.opacity > 0)
      p.style.opacity -= .1;
    else {
      clearInterval(p2);
      p.style.visibility = "hidden";
    }
  }, 60)
}