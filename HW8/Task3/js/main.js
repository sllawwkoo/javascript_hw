/* Сздайте картинку и кнопку с названием "Изменить картинку" сделайте так чтобы при загрузке страницы
 была картинка https://itproger.com/img/courses/1476977240.jpg При нажатии на кнопку первый раз
  картинка заменилась на https://itproger.com/img/courses/1476977488.jpg при втором нажатии чтобы картинка заменилась на 
  https://upload.wikimedia.org/wikipedia/commons/thumb/9/99/Unofficial_JavaScript_logo_2.svg/1200px-Unofficial_JavaScript_logo_2.svg.png */

//Создаём первый элемент - div   
const div1 = document.createElement("div1");
document.body.prepend(div1);
div1.classList.add("div1");

//В соданный div создаём, дополняем стилями и помещяем элемент - img 
const img1 = document.createElement("img");
document.querySelector('div1').append(img1);
img1.setAttribute('src', 'https://itproger.com/img/courses/1476977240.jpg');
img1.classList.add('img1');

//Создаём вотрой элемент - div 
const div2 = document.createElement("div");
div1.after(div2);
div2.classList.add('div1', 'div2');

//Во второй div, создаём, дополняем стилями и помещяем элемент - input
const inputBtn = document.createElement('input');
document.querySelector('body > div').append(inputBtn);
inputBtn.setAttribute("type", 'button');
inputBtn.setAttribute('value', 'Изменить картинку');
inputBtn.classList.add('inputs');

inputBtn.addEventListener('click', () => {
    if (img1.getAttribute('src') == 'https://itproger.com/img/courses/1476977488.jpg') {
        img1.setAttribute('src', 'https://upload.wikimedia.org/wikipedia/commons/thumb/9/99/Unofficial_JavaScript_logo_2.svg/1200px-Unofficial_JavaScript_logo_2.svg.png');
    } else { img1.setAttribute('src', 'https://itproger.com/img/courses/1476977488.jpg'); }
});



