/* Создайте 2 инпута и одну кнопку. Сделайте так чтобы инпуты обменивались содержимым. */

// Создаём три элемента div
const div1 = document.createElement("div");
document.body.prepend(div1);
div1.classList.add("div1");

const div2 = document.createElement("div");
div1.after(div2);
div2.classList.add("div1");

const div3 = document.createElement("div");
div2.after(div3);
div3.classList.add("div1");

//Создаём два элемента input 
const input1 = document.createElement('input');
document.querySelector('div:nth-child(1)').append(input1);
input1.setAttribute('type', 'text');
input1.setAttribute('id', 'input1');
input1.setAttribute('value', ' ');
input1.classList.add('inputs');

const input2 = document.createElement('input');
document.querySelector('div:nth-child(2)').append(input2);
input2.setAttribute('type', 'text');
input2.setAttribute('id', 'input2');
input2.setAttribute('value', ' ');
input2.classList.add('inputs');

//Сщздаём элемент input "кнопка"
const inputBtn = document.createElement('input');
document.querySelector('div:nth-child(3)').append(inputBtn);
inputBtn.setAttribute('type', 'button');
inputBtn.setAttribute('value', 'exchange');
inputBtn.setAttribute('id', 'btn');
inputBtn.classList.add('button');

const btn = document.getElementById('btn');

btn.addEventListener('click', () => {
    const getText = input1.value;
    if (getText != '') {
        input2.value = input1.value;
        input1.value = '';
    } else {
        input1.value = input2.value;
        input2.value = '';
    }
});


